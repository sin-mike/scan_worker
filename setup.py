#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from setuptools import setup, find_packages

import sys

readme = """Process PDF documents looking for Exam Tools barcodes"""
version = '0.1'

setup(
    name='scan-worker',
    version=version,
    url='http://oly-exams.org',
    author='Oly Exams Team',
    author_email='michele.dolfi@oly-exams.org',
    description=readme,
    install_requires=['requests','Wand','zbar','Pillow','PyPDF2'],
    scripts=['scan-worker.py'],
    license='Apache 2.0',
)